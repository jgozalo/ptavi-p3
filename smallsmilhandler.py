#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class SmallSMILHandler(ContentHandler):
    def __init__(self):
        """
        Constructor.
        """
        self.list = []
        # Diccionario de diccionarios
        self.smil = {
            'root-layout': {
                'name': "",
                'width': "",
                'height': "",
                'background-color': ""
            },
            'region': {
                'name': "",
                'id': "",
                'top': "",
                'bottom': "",
                'left': "",
                'right': ""
            },
            'img': {
                'name': "",
                'src': "",
                'region': "",
                'begin': "",
                'dur': ""
            },
            'audio': {
                'name': "",
                'src': "",
                'begin': "",
                'dur': ""
            },
            'textstream': {
                'name': "",
                'src': "",
                'region': ""
            }
        }

    def get_tags(self, parteDiccionario):
        return self.list.append(parteDiccionario)

    def startElement(self, name, attrs):
        """
        Método que se llama cuando se abre una etiqueta
        """
        if name == 'root-layout':
            # De esta manera tomamos los valores de los atributos
            self.smil['root-layout']['width'] = attrs.get('width', "")
            self.smil['root-layout']['height'] = attrs.get('height', "")
            self.smil['root-layout']['background-color'] = \
                attrs.get('background-color', "")
            diccAux = {'root-layout': self.smil['root-layout']}
            self.get_tags(diccAux)
        elif name == 'region':
            self.smil['region']['id'] = attrs.get('id', "")
            self.smil['region']['top'] = attrs.get('top', "")
            self.smil['region']['bottom'] = attrs.get('bottom', "")
            self.smil['region']['left'] = attrs.get('left', "")
            self.smil['region']['rigth'] = attrs.get('right', "")
            diccAux = {'region': self.smil['region']}
            self.get_tags(diccAux)
        elif name == 'img':
            self.smil['img']['src'] = attrs.get('src', "")
            self.smil['img']['region'] = attrs.get('region', "")
            self.smil['img']['begin'] = attrs.get('dur', "")
            self.smil['img']['dur'] = attrs.get('dur', "")
            diccAux = {'img': self.smil['img']}
            self.get_tags(diccAux)
        elif name == 'audio':
            self.smil['audio']['src'] = attrs.get('src', "")
            self.smil['audio']['begin'] = attrs.get('begin', "")
            self.smil['audio']['dur'] = attrs.get('dur', "")
            diccAux = {'audio': self.smil['audio']}
            self.get_tags(diccAux)
        elif name == 'textstream':
            self.smil['textstream']['src'] = attrs.get('src', "")
            self.smil['textstream']['region'] = attrs.get('region', "")
            diccAux = {'textstream': self.smil['textstream']}
            self.get_tags(diccAux)


if __name__ == "__main__":
    """
        Programa principal
        """
    parser = make_parser()
    sHandler = SmallSMILHandler()
    parser.setContentHandler(sHandler)
    parser.parse(open('ptavi-p3/karaoke.smil'))
    print(sHandler.list)

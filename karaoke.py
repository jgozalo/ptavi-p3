import json
import sys
from xml.sax import make_parser
from smallsmilhandler import SmallSMILHandler
import json
import urllib.request


class KaraokeLocal(SmallSMILHandler):
    def __init__(self, file_name):
        super().__init__()
        self.file_name = file_name
        parser = make_parser()
        parser.setContentHandler(self)
        parser.parse(open(self.file_name))

    def __str__(self):
        """
        Devuelve una lista con los strings a imprimir
        """
        lineaTotal = []
        for lineas in self.list:
            elementos = lineas.items()
            for c, v in elementos:
                lineaElemento = c + "\\"
                for cc, vv in v.items():
                    if vv != "":
                        lineaElemento += cc + '"' + vv + '"\\'
                lineaTotal.append(lineaElemento)
        return lineaTotal

    def do_local(self):
        for local_lineas in self.list:
            elementos = local_lineas.items()
            for c, v in elementos:
                for cc, vv in v.items():
                    if vv[:7] == "http://":
                        barra_Cruzada = vv.rfind("/")
                        print("Descargando: " + vv[barra_Cruzada + 1:]
                              + " en: " +
                              "/home/alumnos/jgozalo/PROTOCOLOS/" +
                              vv[barra_Cruzada + 1:])

                        urllib.request.urlretrieve(vv,
                                                   "/home/alumnos/jgozalo/"
                                                   "PROTOCOLOS/"
                                                   + vv[barra_Cruzada + 1:])

    def to_json(self, nombre_fich_smil="", nombre_fich_json=""):
        if nombre_fich_json == "":
            nombre_fich_json = nombre_fich_smil[:-5]
        with open(nombre_fich_json + '.json', 'w') as f:
            json.dump(self.list, f, indent=4)


if __name__ == "__main__":
    try:
        f_name = sys.argv[1]
    except ValueError:
        sys.exit("Usage: python3 karaoke.py file.smil")

    karLoc = KaraokeLocal(f_name)
    for lineas in karLoc.__str__():
        print(lineas[:-1])
    # con un argumento
    karLoc.to_json(f_name)

    karLoc.do_local()

    # con dos argumentos
    karLoc.to_json(f_name, "otro")

    for lineas in karLoc.__str__():
        print(lineas[:-1])
